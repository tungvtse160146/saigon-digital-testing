import type { Config } from "tailwindcss";
import typography from "@tailwindcss/typography";

export default {
  content: [
    "./app/**/*.{ts,tsx}",
    "./components/**/*.{ts,tsx}",
    "./lib/**/*.{ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["var(--font-inter)"],
        poppins: ['Poppins']
      },
      textColor: {
        sd: "#3285E1"
      },
      backgroundColor: {
        sd: "#3285E1"
      },
      borderColor: {
        sd: "#3285E1"
      },
      height: {
        hero: "720px",
        intro: "2100px"
      },

    },
  },
  future: {
    hoverOnlyWhenSupported: true,
  },
  plugins: [typography],
} satisfies Config;
