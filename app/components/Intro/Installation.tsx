import { AppContext } from '@/app/Context/AppContext'
import { Poppins } from 'next/font/google'
import React, { useContext } from 'react'

type Props = {}

const poppins = Poppins({
    subsets: ['latin'],
    weight: '400'
})

const Installation = (props: Props) => {
    const { dataLandingPage }: any = useContext(AppContext)
    const logoCh = dataLandingPage && dataLandingPage.props.landingPage?.find((item: any) => item.fields.slug == 'logo-ch').fields.featuredImage.fields.file.url
    const logoApple = dataLandingPage && dataLandingPage.props.landingPage?.find((item: any) => item.fields.slug == 'logo-apple').fields.featuredImage.fields.file.url
    return (
        <div className='absolute  transition-all ease-in-out duration-200 z-20 md:top-[2%] md:left-[25%] lg:top-[16%] lg:left-[21%] '>
            <div className='flex flex-col gap-2 max-w-[456px] p-2'>
                <p className={`${poppins.className} text-sd font-medium text-2xl`}>THE BENEFIT WE HAVE</p>
                <p className={`${poppins.className} text-black !font-bold text-[40px]`}>Revolutionize with Effortless Digital Namecard Exchange!</p>
            </div>
            <div className='flex gap-8 mt-5 lg:mt-6'>
                <button className={`${poppins.className} bg-black hover:bg-gray-700 text-white text-sm px-2 border border-black rounded-md`}>
                    <div className={`flex gap-1 justify-center items-center ${poppins.className}`}>
                        <img src={logoApple} className='w-6 h-8' />
                        <div className='flex flex-col gap-0 text-start'>
                            <p className={`${poppins.className} text-[10px]`}>Download on the</p>
                            <p className={`${poppins.className} text-xl !font-semibold pb-2`}>App Store</p>
                        </div>
                    </div>
                </button>
                <button className={`${poppins.className} bg-black hover:bg-gray-700 text-white text-sm p-2 border border-black rounded-md`}>
                    <div className='flex gap-2 justify-center items-center'>
                        <img src={logoCh} className='w-6 h-8' />
                        <div className='flex flex-col gap-0 text-start'>
                            <p className={`${poppins.className} text-[10px] items-end`}>GET IT ON</p>
                            <p className={`${poppins.className} text-xl !font-semibold pb-2`}>Google Play</p>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    )
}

export default Installation