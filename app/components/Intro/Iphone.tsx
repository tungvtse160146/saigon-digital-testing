import { AppContext } from '@/app/Context/AppContext'
import React, { useContext } from 'react'

type Props = {}

const Iphone = (props: Props) => {
  const { dataLandingPage }: any = useContext(AppContext)

  const iphone13 = dataLandingPage && dataLandingPage.props.landingPage?.find((item: any) => item.fields.slug == 'phone').fields.featuredImage.fields.file.url

  return (
    <div className='relative'>
      <div id="solar" className="transition-all ease-in-out duration-200 absolute table-cell align-middle mx-auto">
        <div className="in relative w-96 h-96 flex items-center justify-center scale-[0.75]">
          <div className=" z-30 border-blue-300 rounded-full border xl:w-[478px] xl:h-[478px] md:w-[300px] md:h-[300px]  bg-gradient-to-r from-white to-[#E8E9F4] flex items-center justify-center absolute animate-spin" style={{
          }}>
          </div>
          <div className=" z-20 border-blue-300 rounded-full border xl:w-[675px] xl:h-[675px] md:w-[500px] md:h-[500px] bg-c bg-gradient-to-r from-white to-[#E8E9F4] flex items-center justify-center absolute animate-spin" style={{
            animationDuration: "20s"
          }}>
            <div className="absolute -top-3 text-white text-sm flex flex-col items-center">

              <div className="rounded-full h-7 w-7 bg-gradient-to-r from-[#6A3E97] to-[#1471B8]"></div>
            </div>
          </div>
          <div className="z-1 rounded-full border border-blue-300 xl:w-[878px] xl:h-[878px] md:w-[680px] md:h-[680px] bg-gradient-to-r from-white to-[#E8E9F4] flex items-center justify-center absolute animate-spin" style={{
            animationDuration: "150s"
          }}>
            <div className="absolute -top-3 text-white text-sm flex flex-col items-center">
              <div className="rounded-full h-[2rem] w-[2rem] bg-gradient-to-r from-[#579FD7] to-[#0F73BA]"></div>
            </div>
          </div>
          <div className=' absolute z-30'>
            <img className='md:w-96 transition-height' src={iphone13} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Iphone