import { AppContext } from '@/app/Context/AppContext';
import React, { useContext, useEffect, useState } from 'react'
import CardItem from '../CardItem';

type Props = {}

type Card = {
    title: string,
    logo: string,
    description: string
}

const CardItemSection = (props: Props) => {
    const { cardItem }: any = useContext(AppContext)
    const [dataRender, setDataRender] = useState<Card[]>([])
    useEffect(() => {
        if (cardItem && cardItem.props.cardItem) {
            console.log('gavl');

            const data = cardItem?.props.cardItem.map((item: any) => {
                return {
                    title: item.fields.title,
                    logo: item.fields.icon.fields.file.url,
                    description: item.fields.description
                }
            })

            setDataRender(data)
        }

    }, [cardItem])

    return (
        <div className='flex transition-all ease-in-out duration-200 flex-col items-center justify-center gap-10 md:pt-24 md:flex-row md:justify-center md:flex-wrap lg:px-[12%]'>
            {dataRender && dataRender.length && (
                <>
                    {dataRender.map((item: Card) => {
                        return (
                            <CardItem logo={item.logo} title={item.title} description={item.description} />
                        )
                    }).reverse()}
                </>
            )}
        </div>
    )
}

export default CardItemSection