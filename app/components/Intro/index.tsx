"use client"
import { AppContext } from '@/app/Context/AppContext'
import { Poppins } from 'next/font/google'
import React, { useContext } from 'react'
import BoxMessage from '../BoxMessage'
import Installation from './Installation'
import Iphone from './Iphone'
import CardItem from '../CardItem'
import CardItemSection from './CardItemSection'

type Props = {
}

const poppins = Poppins({
    subsets: ['latin'],
    weight: '400'
})

const IntroSection = (props: Props) => {
    const { dataLandingPage, dataIntro }: any = useContext(AppContext)
    const introImage = dataLandingPage && dataLandingPage.props.landingPage?.find((item: any) => item.fields.slug == 'intro-image').fields.featuredImage.fields.file.url
    const bgEllipse = dataLandingPage && dataLandingPage.props.landingPage?.find((item: any) => item.fields.slug == 'background-el').fields.featuredImage.fields.file.url
    const bgEllipse2 = dataLandingPage && dataLandingPage.props.landingPage?.find((item: any) => item.fields.slug == 'background-el2').fields.featuredImage.fields.file.url
    const message1 = dataIntro && dataIntro.props.introInFo?.find((item: any) => item.fields.title == 'IntroInfo').fields.message1
    const message2 = dataIntro && dataIntro.props.introInFo?.find((item: any) => item.fields.title == 'IntroInfo').fields.message2

    return (
        <>
            <div className={`relative h-intro lg:h-[1750px]`}>
                <Installation />
                <div className='absolute  transition-all ease-in-out duration-200 top-[25%] md:left-[20%] lg:top-[8%] lg:left-[52%] z-20  '>
                    <img src={introImage} className='xl:w-[552px] transition-height h-[680px]' />
                </div>
                <div className='absolute transition-all ease-in-out duration-200 top-[40%] md:left-[20%] md:top-[25%] lg:top-[4%] lg:left-[58%] z-19  '>
                    <img src={bgEllipse} className='lg:size-max ' />
                </div>
                <div className='absolute xl:top-[2%] transition-all ease-in-out duration-200 lg:top-[8%] top-[20%] left-[2%] z-19  '>
                    <img src={bgEllipse2} className='lg:size-max ' />
                </div>
                <div className="table absolute transition-all ease-in-out duration-200 top-[62%] left-5 md:left-[25%] md:top-[72%] lg:top-[68%] lg:left-[42%] h-full mx-auto">
                    <Iphone />
                </div>
                <div className='absolute transition-all ease-in-out duration-200 z-90 top-[90%] left-[5%] md:top-[60%] md:left-0 lg:top-[55%] lg:left-[15%] xl:left-[20%]'>
                    <BoxMessage message={message2} corner='br' />
                </div>
                <div className='absolute transition-all ease-in-out duration-200 z-90 top-[85%] left-[5%] md:top-[90%] md:left-[60%] lg:top-[86%] lg:left-[66%]'>
                    <BoxMessage message={message1} corner='tl' />
                </div>
            </div>
            <CardItemSection />

        </>
    )
}

export default IntroSection