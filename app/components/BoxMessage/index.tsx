"use client"
import { AppContext } from '@/app/Context/AppContext'
import { Poppins } from 'next/font/google'
import React from 'react'

type Props = {
    message: string,
    corner: string,
}

const poppins = Poppins({
    subsets: ['latin'],
    weight: '400'
})

const BoxMessage = (props: Props) => {
    const { message, corner } = props
    return (
        <div className={`max-w-[360px] bg-white ${corner === 'br' ? 'rounded-t-full rounded-bl-full' : 'rounded-b-full rounded-tr-full'} shadow-2xl p-6`}>
            <p className={`${poppins.className} text-[#64A9F5] text-sm`}>{message}</p>
        </div>
    )
}

export default BoxMessage