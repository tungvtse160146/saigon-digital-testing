"use client"
import React, { useContext } from 'react';
import { Poppins } from 'next/font/google';
import { AppContext } from '@/app/Context/AppContext';

const poppins = Poppins({
    weight: '500',
    subsets: ['latin'],
})

type Props = {
};

const Header: React.FC<Props> = () => {
    const { dataLandingPage }: any = useContext(AppContext)
    const image = dataLandingPage && dataLandingPage.props.landingPage.find((item: any) => item.fields.slug === 'header')
    return (
        <div className='flex bg-white py-6'>
            <div className='basis-1/2 justify-center flex items-center align-middle'>
                <img src={image?.fields.featuredImage.fields.file.url} />
            </div>
            <div className='basis-1/2 justify-center flex items-center align-middle gap-4'>
                <p className={`${poppins.className}`}>
                    Ready to changing?
                </p>
                <button className="bg-transparent hover:bg-sd text-sd font-semibold hover:text-white py-3 px-6 text-[16px] border border-sd hover:border-transparent rounded-full">
                    <p className={`${poppins.className}`}>Insta Benefit</p>
                </button>
            </div>
        </div>
    );
};

export default Header;
