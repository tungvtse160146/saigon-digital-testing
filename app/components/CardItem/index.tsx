import { AppContext } from '@/app/Context/AppContext'
import { Poppins } from 'next/font/google'
import React, { useContext } from 'react'

type Props = {
    title: string,
    logo: string,
    description: string
}

const poppins = Poppins({
    subsets: ['latin'],
    weight: '400'
})

const CardItem = (props: Props) => {
    const { title, logo, description } = props
    return (
        <div style={{
            boxShadow: 'inset 0px 5.45455px 27.2727px #ABE1F2'

        }} className="bg-opacity-3 transition-all ease-in-out duration-200 text-center min-h-64 rounded-md max-w-[264px] flex flex-col justify-evenly items-center p-4 md:basis-1/2">
            <img src={logo} />
            <p className={`${poppins.className} text-2xl !font-semibold text-[#1C1C1C]`}>{title}</p>
            <p className={`${poppins.className} text-xl text-[#1C1C1C]`}>
                {description}
            </p>
        </div>
    )
}

export default CardItem