import React from 'react';
import { ConfigProvider, Slider } from 'antd';
import { SliderSingleProps } from 'antd/es/slider';

class CustomSlider extends React.Component<SliderSingleProps> {
    render() {

        return <ConfigProvider
            theme={{
                components: {
                    Slider: {
                        railBg: 'white',
                        railSize: 14,
                        trackBg: '#05497A',
                        trackHoverBg: '#05497A',
                        railHoverBg: 'white',
                        handleColor: '#1C1C1C',
                        handleActiveColor: '#1C1C1C',
                        handleSize: 20
                    },

                },
                token: {
                    borderRadiusXS: 16
                }
            }}
        >
            <Slider className=' w-64' {...this.props} />
        </ConfigProvider>;
    }
}

export default CustomSlider;