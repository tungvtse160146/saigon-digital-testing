"use client"
import React, { useContext } from 'react'
import { Poppins } from 'next/font/google'
import Mail from '@/app/Icons/mail'
import { AppContext } from '@/app/Context/AppContext'

type Props = {
}

const poppins = Poppins({
    subsets: ['latin'],
    weight: '400'
})

const HeroSection = (props: Props) => {
    const { dataLandingPage }: any = useContext(AppContext)
    const url = dataLandingPage && dataLandingPage.props.landingPage?.find((item: any) => item.fields.slug == 'hero-section').fields.featuredImage.fields.file.url
    const wave = dataLandingPage && dataLandingPage.props.landingPage?.find((item: any) => item.fields.slug == 'wave-hero').fields.featuredImage.fields.file.url
    return (
        <div className={`relative h-hero w-full bg-[url('//images.ctfassets.net/5s7w2h6zbb4f/3XDlvL0UmncFvV5UNGpcF3/dc9d4d3fffc008caa1da20b7f37d46eb/background-hero.png')]`}>
            <div className='absolute transition-all ease-in-out duration-400 z-20 md:top-[10%] md:left-[20.5%] lg:top-[17%]'>
                <div className='flex flex-col gap-2 p-2 '>
                    <p className={`${poppins.className} text-sd !font-bold text-5xl`}>Say goodbye</p>
                    <p className={`${poppins.className} text-black !font-bold text-5xl`}>to Business cards</p>
                    <p className={`${poppins.className} text-black text-2xl lg:pt-6 pt-0`}>Effortless contact exchange</p>
                    <div className='pt-2 lg:mt-[12px]'>
                        <div className='flex h-16 bg-white gap-16 items-center rounded-full pr-[10px]'>
                            <div className='text-[#9A9EA6] flex gap-2 basis-2/3 pl-5 justify-center items-center'>
                                <Mail />
                                <input type='text' className=' rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline' placeholder='Enter your email' />
                            </div>
                            <button className={`${poppins.className} md:w-[184px] w-28 md:p-[10px] p-1 bg-sd md:text-[16px] text-sm hover:bg-blue-700 text-white border border-sd rounded-full`}>
                                Get Early Access
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className='absolute transition-all ease-in-out duration-400 bottom-0 z-20 md:left-[30%] lg:left-[55%] '>
                <img src={url} className='lg:size-max size-[400px]' />
            </div>
            <div className='absolute transition-all ease-in-out duration-400 bottom-[12%] z-10 opacity-35'>
                <img src={wave} />
            </div>
        </div>
    )
}

export default HeroSection