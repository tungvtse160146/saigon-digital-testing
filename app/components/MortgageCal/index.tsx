"use client"
import { Poppins } from 'next/font/google'
import React, { useEffect, useMemo, useState } from 'react'
import CustomSlider from '../CustomSlider'

type Props = {}

const poppins = Poppins({
  subsets: ['latin'],
  weight: '400'
})


const MortgageCal = (props: Props) => {
  const [purchase, setPurchase] = useState(0)
  const [downPayment, setDownPayment] = useState(0)
  const [isDisableDownPayment, setIsDisableDownPayment] = useState(false)
  const [rePaymentTime, setRePaymentTime] = useState(0)
  const [rate, setRate] = useState(0)
  const loan = useMemo(() => {
    return purchase - downPayment
  }, [purchase, downPayment])

  useEffect(() => {
    if (downPayment > purchase) {
      setIsDisableDownPayment(true)
    }
    else {
      setIsDisableDownPayment(false)
    }
  }, [downPayment, purchase])

  const paymentPerMonth = useMemo(() => {
    return loan * (rate / 100 * Math.pow((1 + rate / 100), rePaymentTime) / (Math.pow((1 + rate / 100), rePaymentTime) - 1))
  }, [loan, rePaymentTime, rate])

  return (
    <div className=' transition-all ease-in-out duration-200 flex flex-col gap-3 justify-center items-center p-2 lg:flex-row lg:my-40 lg:px-[12%]'>
      <div className='max-w-[456px] flex flex-col gap-6 justify-center items-left lg:basis-1/2 lg:pl-12 lg:pb-20'>
        <p className={`${poppins.className} text-[#1C1C1C] !font-semibold text-[40px] `}>Try our awesome Calculator</p>
        <p className={`${poppins.className} text-[#585C65] text-[18px]`}>Adjust the purchase price, down payment, and interest rate to fit your budget.</p>
        <button className={`${poppins.className} w-[184px] py-3 bg-sd text-[16px] hover:bg-blue-700 text-white border border-sd rounded-full`}>
          Register
        </button>
      </div>
      <div className='flex flex-col gap-4 bg-sd p-12 lg:basis-1/2'>
        <p className={`${poppins.className} text-white text-[40px] !font-semibold`}>
          Mortgage Calculator
        </p>
        <p className={`${poppins.className} text-white text-[18px] `}>
          Take control with a plan set in stone.
        </p>
        <div className='flex flex-col gap-3 pt-12 lg:flex-row lg:flex-wrap lg:gap-0 max-w-[650px]  '>
          <div className="flex flex-col gap-0 basis-1/2">
            <p className={`${poppins.className} text-white text-[18px]`}>
              Purchase Price: ${purchase.toLocaleString('es')}
            </p>
            <CustomSlider min={0} max={1000000} onChange={(value: any) => {
              setPurchase(value)
            }} />
          </div>
          <div className="flex flex-col gap-0 basis-1/2">
            <p className={`${poppins.className} text-white text-[18px] `}>
              Down Payment: ${downPayment.toLocaleString('es')}
            </p>
            <CustomSlider disabled={isDisableDownPayment} min={0} max={400000} onChange={(value: any) => setDownPayment(value)} />

          </div>
          <div className="flex flex-col gap-0 basis-1/2">
            <p className={`${poppins.className} text-white text-[18px] `}>
              Repayment time: {rePaymentTime} years
            </p>
            <CustomSlider min={0} max={60} onChange={(value: any) => setRePaymentTime(value)} />

          </div>
          <div className="flex flex-col gap-0 basis-1/2">
            <p className={`${poppins.className} text-white text-[18px] `}>
              Interest Rate: {rate}%
            </p>
            <CustomSlider min={0} max={20} onChange={(value: any) => setRate(value)} />
          </div>

        </div>
        <div>
          <div className='flex gap-2 md:pt-12'>
            <p className={`${poppins.className} text-[18px] text-white`}>Loan amount:</p>
            <p className={`${poppins.className} text-2xl text-[#1C1C1C]`}>${loan > 0 && loan.toLocaleString('es')}</p>
          </div>
          <div className='flex gap-2'>
            <p className={`${poppins.className} text-[18px] text-white`}>Estimated repayment per month:</p>
            <p className={`${poppins.className} text-2xl text-[#1C1C1C]`}>${!isNaN(paymentPerMonth) && Math.round(paymentPerMonth).toLocaleString('es')}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default MortgageCal