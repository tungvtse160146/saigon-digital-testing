"use client"
import { getIntroInfo, getLandingPage } from "@/lib/api";
import Header from "./components/Header";
import HeroSection from "./components/HeroSection";
import React, { useEffect, useState } from "react";
import IntroSection from "./components/Intro";
import { AppProvider } from "./Context/AppContext";
import MortgageCal from "./components/MortgageCal";




export default function Page() {
  const [dataLandingPage, setDataLandingPage] = useState<any>(null);
  const [dataIntro, setDataIntro] = useState<any>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const landingPageData = await getLandingPage();
        const introData = await getIntroInfo();

        setDataLandingPage(landingPageData);
        setDataIntro(introData);
      } catch (error) {
      }
    };

    fetchData();
  }, []);

  return (
    <div className="">
      <AppProvider>
        <Header />
        <HeroSection />
        <IntroSection />
        <MortgageCal />
      </AppProvider>

    </div>
  );
}
