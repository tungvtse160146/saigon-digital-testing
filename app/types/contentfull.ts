export interface contentFulData {
    fields: TFields,
    metaData: any,
    sys: any
}

export interface TFields {
    title: string,
    featuredImage: TImage,
    slug: string,
    thumbnail: TImage
}

export interface TImage {
    fields: {
        description: string,
        file: {
            url: string,
        },
        title: string
    }
}