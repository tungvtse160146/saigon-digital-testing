import { getCardItem, getIntroInfo, getLandingPage } from "@/lib/api";
import React, { createContext } from "react";

export const AppContext = createContext({})

export const AppProvider = ({ children }: any) => {
    const [dataLandingPage, setDataLandingPage] = React.useState<any>(null);
    const [dataIntro, setDataIntro] = React.useState<any>(null);
    const [cardItem, setCardItem] = React.useState<any>(null)

    React.useEffect(() => {
        const fetchData = async () => {
            try {
                const landingPageData = await getLandingPage();
                const introData = await getIntroInfo();
                const cardItem = await getCardItem()

                setDataLandingPage(landingPageData);
                setDataIntro(introData);
                setCardItem(cardItem)
            } catch (error) {
                console.log("error", error);
            }
        };

        fetchData();
    }, []);
    return <AppContext.Provider value={{ dataLandingPage, dataIntro, cardItem }}>
        {children}
    </AppContext.Provider>
}